<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use Yii;


class User extends ActiveRecord implements IdentityInterface
{

    public $role;
	public $auth_key;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc} 
     */
    public function rules()
    {
        return [
            [['username', 'password', 'role'], 'required'],
            [['username'], 'unique'],
            [['username', 'password'], 'string', 'max' => 20],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'password' => 'Пароль',
            'role' => 'Роль',
        ];
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    /**
     * Прароль не хеширован!!! Хранится открытым текстом.
     *
     * @param $username
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($username, $password)
    {
        if (self::findOne(['username' => $username])->getAttribute('password') === $password) {
            return true;
        }
        return false;
    }

    /**
     * Назначаем роль после сохранения
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        Yii::$app->authManager->revokeAll($this->getId());
        $userRole = \Yii::$app->authManager->getRole($this->role);
        Yii::$app->authManager->assign($userRole, $this->getId());
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        Yii::$app->authManager->revokeAll($this->getId());
        return parent::beforeDelete();
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return self::findOne(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
	
	
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
	
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
          return static::findOne(['access_token' => $token]);
    }

}